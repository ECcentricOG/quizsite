
const quiz = [

    {
        q:'Wite a java program to print a series of prime numbers from entered range i/p = 10   100  o/p = 11 13 17.....97',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program to take a numbers as input and print addition of factorials of each digit from that number i/p=1234 o/p=addition of factorials of each digit from 1234 is 33'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program and takes 2 charcters if these characters are equal then print them as it is but if they are unequal then print their difference'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program,in which according to month No print the No of days in that month'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program to find maximum between 3 numbers'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Calculate profit or loss, Write a program that takes Cost and Selling price and calculate its profit or loss'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program to print all even numbers in reverse order and odd number in std way i/p=2  9  0/p= 8 6 4 2    3 5 7 9'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program which accept 3 numbers and check whether they are Pythagorean Triplets or not'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a unique real life example of if elseif else ladder'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program That Takes input from BufferedReader which must include String , Character ,Integer , Floating Values'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program for real life Switch Case Example'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program which takes Input in one String and gives output as tokens (must include one character) '],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Create a method in java to identify a given number is Prime or not'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Create a method in java to identify a given number is Strong or not'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Print the sum of first 10 numbers'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Print the product of first 10 numbers'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program to calculate factorial of given number'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program to count the digit from following number'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program to print square of even digits from number'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program to print sum of all even numbers and product of odd numbers from 1 to 10'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program which takes number from user if number is even print that number in reverse order or if odd print in reverse with difference of 2'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Create a method in java to identify a given number is Armstrong or not'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program to reverse the given number'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Check whether given number is Palindrome number or not'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program to check for a given number is Prime Number'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program for Automorphic Number  (5 = 25 , 25 = 625 , 6 = 36)'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    }, 
    {
        q:['Write a program Strong Number (sum of factors of each digit is equal to number ex.145 = 1+24+120)'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program Armstrong Number (if no is 3 digit then sum of all digits cube is equal to number if 2 digit no then square)'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Write a program Perfect Number (Sum of all factors excluding itself is equal to same no then it is perfect ex. 6 = 1+2+3)'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Given an integer A which represent unit of electricity consumed at your house claculate total amt unit<=100 - 1rs,unit>100 - 2rs'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Belive In YourSelf'],
        options:['Keep Going No Other Option'],
        answer:0
    },
    {
        q:['Print all numbers from 1 to 50 except those who are divisible by 3 and 5 or 4'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Which of the following is a correct header line for main in Java program ?'],
        options:['public static void main(args)','public void static main (String [] args)','public static void main(String [])','public static void main(String [] args)'],
        answer:3
    },
    {
        q:['What is old name for Java Programming ?'],
        options:['Java2','Oak','java'],
        answer:1
    },
    {
        q:['_______ is the developer of the java programming language'],
        options:['Dennis Ritchie','James Gosling','Bjarne Stroustrup'],
        answer:1
    },
    {
        q:['What is the file extension of java source code ?'],
        options:['.java','.class','.exe'],
        answer:0
    },
    {
        q:['When was Java released ?'],
        options:['1975','1985','1995'],
        answer:2
    },
    {
        q:['Which one is not part of JVM Architecture ?'],
        options:['Classloader Subsystem','Native Method Library','Java Compiler'],
        answer:2
    },
    {
        q:['Which one is not part of the Execution Engine ?'],
        options:['Interpreter','JNI','JIT','Garbage Collector'],
        answer:1
    },
    {
        q:['What is the file extension for Java Executable Code ?'],
        options:['.java','.class','.exe'],
        answer:1
    },
    {
        q:['long form of JRE is _____'],
        options:['jdk running environment','java runtime environment','jre running environment'],
        answer:1
    },
    {
        q:['java is ______'],
        options:['process oriented language','low level language','object oriented programming language','non robust'],
        answer:2
    },
    {
        q:['Latest version of Java is '],
        options:['21','18','19','11'],
        answer:2
    },
    {
        q:['long from of JVM is _______'],
        options:['java metaspace','jdk virtual machine','jre virtual machine','java virtual machine'],
        answer:3
    },
    {
        q:['long form of JDK is _______'],
        options:['jdk running envt','java development tool','java development kit'],
        answer:2
    },
    {
        q:['JVM is also known as '],
        options:['Compiler','mini operating system','pvm'],
        answer:1
    },
    {
        q:['Tick the Correct Statement'],
        options:['Java Byte code is platform-dependant','Java Byte code is platform-independant','java is non robust','all of the above'],
        answer:1
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U1.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U2.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/P3.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/P4.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/P5.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U6.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U7.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U8.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U9.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U10.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U11.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U12.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U13.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U14.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U15.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U16.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U17.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U18.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U19.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U20.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U21.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U22.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U23.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U24.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U25.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U26.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U27.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U28.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U29.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U30.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U31.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U32.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U33.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U34.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U35.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U36.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U37.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U38.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U39.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U40.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U41.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U42.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U43.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U44.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U45.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U46.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U47.jpg'
    },
    {
        q:'Print the following Pattern',
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0,
        img:'Img/Question/Pattern/U48.jpg'
    },
    {
        q:'Choose the correct option',
        options:['true','compile time error','false','null'],
        answer:2,
        img:'Img/Question/DataTypes/o9.jpg'
    },
    {
        q:'Choose the correct option',
        options:['12','17','20','9'],
        answer:2,
        img:'Img/Question/DataTypes/o3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['-10','-11','10','11'],
        answer:1,
        img:'Img/Question/DataTypes/o2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['b=3','Syntax error','10','3'],
        answer:3,
        img:'Img/Question/DataTypes/o7.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Infinity','Devide by zero exception','compile time error','none'],
        answer:1,
        img:'Img/Question/DataTypes/o10.jpg'
    },
    {
        q:'Choose the correct option',
        options:['2','24','4','5'],
        answer:2,
        img:'Img/Question/DataTypes/o4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10 10 45','10 45 45','45 45 45','compile time error'],
        answer:2,
        img:'Img/Question/DataTypes/o1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Devide by zero exception','infinity','compile time erroe','none'],
        answer:1,
        img:'Img/Question/DataTypes/o8.jpg'
    },
    {
        q:'Choose the correct option',
        options:['13','3','6','Syntax error'],
        answer:2,
        img:'Img/Question/DataTypes/o5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['11 10 -10 -11','10 10 10 11','11 10 10 10','11 11 -10 -11'],
        answer:0,
        img:'Img/Question/DataTypes/o6.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10 10 11 11 12','10 11 12','10 10 11 11','Compile time error'],
        answer:3,
        img:'Img/Question/For/7.jpg'
    },
    {
        q:'Choose the correct option',
        options:['No Output','10 12 14','40.25 42.25 44.25','Compile time error'],
        answer:3,
        img:'Img/Question/For/4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['1 2 3 4 5 6','1 2 3 4 5 Garbage Value','Compile Time Error','1 2 3 4 5 0'],
        answer:2,
        img:'Img/Question/For/6.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile error : variable i might not have been initialized','Compile error : variable j might not have been initailized','Both Errors','Before for loop   Inside for   After for loop'],
        answer:0,
        img:'Img/Question/For/2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['No Output','5 2 1 1 1','Compile time error','5 1 1 1 1'],
        answer:2,
        img:'Img/Question/For/9.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile Time error','12','15','12 15'],
        answer:3,
        img:'Img/Question/For/1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10 9 8 7 6 5','5 6 7 8 9 10','10 9 8 7 6','None of the Above'],
        answer:3,
        img:'Img/Question/For/8.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10 10 11 11 12','10 11 12','10 10 11 11','Compile time error'],
        answer:3,
        img:'Img/Question/For/3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Before for loop    Inside for loop    After for loop','Before for loop    After for loop','Infinite loop','Compile time error'],
        answer:2,
        img:'Img/Question/For/5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile error : variable i might not have been initialized','Compile error : variable j might not have been initialized','Both Errors','none'],
        answer:2,
        img:'Img/Question/For/10.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Inside if','Inside else','Compile time error','None of the Above'],
        answer:2,
        img:'Img/Question/IfElse/3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['error : value not mapped to a boolean','error : incompatable type : int cannot be convert to boolean','Inside else','Inside if'],
        answer:1,
        img:'Img/Question/IfElse/5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Inside if1','Inside if2','Inside if1    Inside if2'],
        answer:3,
        img:'Img/Question/IfElse/4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Inside else1','Inside else2','Inside if','Compile time error'],
        answer:0,
        img:'Img/Question/IfElse/1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Inside if1','Compile time error','Inside if2','Inside if1    Inside if2'],
        answer:2,
        img:'Img/Question/IfElse/2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['','','',''],
        answer:2,
        img:'Img/Question/IfElse/3.jpg'
    },
    {
        q:['Create a method in java to identify a given number is Pallindrome or not'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Create a method in java to identify a given number is Automorphus or not'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['Create a method in java to identify a given number is Perfect or not'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:'Choose the correct option',
        options:['10    9   8   7   6','10   9   8   7   6   5','1   2   3   4   5','Compile time error'],
        answer:1,
        img:'Img/Question/Loops/3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile Time Error','var = var','var = 10','var = 1   var = 2 ..... var = 10'],
        answer:2,
        img:'Img/Question/Loops/1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10','20','30','Compile time error'],
        answer:3,
        img:'Img/Question/Loops/2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['var = 10','var = 20','run time error','Compile time error'],
        answer:3,
        img:'Img/Question/Loops/6.jpg'
    },
    {
        q:'Choose the correct option',
        options:['var2 = 30.00','var = 30.0','run time error','Compile time error'],
        answer:1,
        img:'Img/Question/Loops/10.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10    9   8   7   6','10','5  4   3   2   1','Compile time error'],
        answer:1,
        img:'Img/Question/Loops/9.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10    9   8   7   6','5   4   3   2   1','none of these','Compile time error'],
        answer:3,
        img:'Img/Question/Loops/7.jpg'
    },
    {
        q:'Choose the correct option',
        options:['var = 30.00','var = 10','none of these','Compile time error'],
        answer:3,
        img:'Img/Question/Loops/8.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10','5','1','error'],
        answer:2,
        img:'Img/Question/Loops/5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['1 error','2 error','3 error','run time error'],
        answer:2,
        img:'Img/Question/Loops/4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Exception','3','run time error'],
        answer:2,
        img:'Img/Question/BasicArray/BA9.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Garbage Value Garbage Value garbage value garbage value garbage value','null null null null','Exception'],
        answer:2,
        img:'Img/Question/BasicArray/BA2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','0 1 2','0 1 1','Exception'],
        answer:0,
        img:'Img/Question/BasicArray/BA10.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Exception','2','4'],
        answer:3,
        img:'Img/Question/BasicArray/BA8.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','0 1','[0 1]','Address of variable'],
        answer:3,
        img:'Img/Question/BasicArray/BA7.jpg'
    },
    {
        q:'Choose the correct option',
        options:['0 1 2 0','Exception','0 1 2','error'],
        answer:1,
        img:'Img/Question/BasicArray/BA4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Exception','1 2 3','0 1 2'],
        answer:0,
        img:'Img/Question/BasicArray/BA5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['error : not a statement','1 2 3 4','error : illegal start of type','Both errors'],
        answer:3,
        img:'Img/Question/BasicArray/BA6.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','10 20 30 40 50 1 2 3 4 5','run time erroe','Exception'],
        answer:1,
        img:'Img/Question/BasicArray/BA1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','0 0 1 2 3','Garbage value 1 2 3','Exception'],
        answer:1,
        img:'Img/Question/BasicArray/BA3.jpg'
    },
    {
        q:['WAP zto take size of array from user and also take integer element from user.Print sum of odd elements only'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['WAP to take size of array from user and also take integer elements from user Print product of all even elements'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['WAP to take size of array from user and also take integer elements from user Print product of odd index only'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['WAP,take 7 characters as input , print only vowles from the array'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:['WAP,take 10 input from the user and print only elements that are divisible by 5'],
        options:['Done','Syntax Error','Wrong Logic'],
        answer:0
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','3','2','4'],
        answer:0,
        img:'Img/Question/BasicArray/BA11.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','0 0 420 0','GV GV GV GV','0 0 0 0'],
        answer:1,
        img:'Img/Question/BasicArray/BA12.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','4 4 4 4','12.0 13.0 14.0 15.0','12 13 4 4'],
        answer:1,
        img:'Img/Question/BasicArray/BA13.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Run Time Exception','0.0','6.0'],
        answer:1,
        img:'Img/Question/BasicArray/BA14.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','1','2','0'],
        answer:2,
        img:'Img/Question/BasicArray/BA15.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Address','true','false'],
        answer:3,
        img:'Img/Question/BasicArray/BA16.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','3','2','GV'],
        answer:2,
        img:'Img/Question/BasicArray/BA17.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','EmptySpace EmptySpace EmptySpace','C 2 W','C garbageVal W'],
        answer:2,
        img:'Img/Question/BasicArray/BA18.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','false','0','1'],
        answer:1,
        img:'Img/Question/BasicArray/BA19.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Address','1 2 3 4 5','1'],
        answer:0,
        img:'Img/Question/BasicArray/BA20.jpg'
    },
    
]