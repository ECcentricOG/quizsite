
const quiz = [
    {
        q:['_______ is the developer of the java programming language'],
        options:['Dennis Ritchie','James Gosling','Bjarne Stroustrup'],
        answer:1
    },
    {
        q:['When was Java released ?'],
        options:['1975','1985','1995'],
        answer:2
    },
    {
        q:['Which one is not part of JVM Architecture ?'],
        options:['Classloader Subsystem','Native Method Library','Java Compiler'],
        answer:2
    },
    {
        q:['Which one is not part of the Execution Engine ?'],
        options:['Interpreter','JNI','JIT','Garbage Collector'],
        answer:1
    },
    {
        q:['JVM is also known as '],
        options:['Compiler','mini operating system','pvm'],
        answer:1
    },
    {
        q:'Choose the correct option',
        options:['true','compile time error','false','null'],
        answer:2,
        img:'Img/Question/DataTypes/o9.jpg'
    },
    {
        q:'Choose the correct option',
        options:['12','17','20','9'],
        answer:2,
        img:'Img/Question/DataTypes/o3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['-10','-11','10','11'],
        answer:1,
        img:'Img/Question/DataTypes/o2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['b=3','Syntax error','10','3'],
        answer:3,
        img:'Img/Question/DataTypes/o7.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Infinity','Devide by zero exception','compile time error','none'],
        answer:1,
        img:'Img/Question/DataTypes/o10.jpg'
    },
    {
        q:'Choose the correct option',
        options:['2','24','4','5'],
        answer:2,
        img:'Img/Question/DataTypes/o4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10 10 45','10 45 45','45 45 45','compile time error'],
        answer:2,
        img:'Img/Question/DataTypes/o1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Devide by zero exception','infinity','compile time erroe','none'],
        answer:1,
        img:'Img/Question/DataTypes/o8.jpg'
    },
    {
        q:'Choose the correct option',
        options:['13','3','6','Syntax error'],
        answer:2,
        img:'Img/Question/DataTypes/o5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['11 10 -10 -11','10 10 10 11','11 10 10 10','11 11 -10 -11'],
        answer:0,
        img:'Img/Question/DataTypes/o6.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10 10 11 11 12','10 11 12','10 10 11 11','Compile time error'],
        answer:3,
        img:'Img/Question/For/7.jpg'
    },
    {
        q:'Choose the correct option',
        options:['No Output','10 12 14','40.25 42.25 44.25','Compile time error'],
        answer:3,
        img:'Img/Question/For/4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['1 2 3 4 5 6','1 2 3 4 5 Garbage Value','Compile Time Error','1 2 3 4 5 0'],
        answer:2,
        img:'Img/Question/For/6.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile error : variable i might not have been initialized','Compile error : variable j might not have been initailized','Both Errors','Before for loop   Inside for   After for loop'],
        answer:0,
        img:'Img/Question/For/2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['No Output','5 2 1 1 1','Compile time error','5 1 1 1 1'],
        answer:2,
        img:'Img/Question/For/9.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile Time error','12','15','12 15'],
        answer:3,
        img:'Img/Question/For/1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10 9 8 7 6 5','5 6 7 8 9 10','10 9 8 7 6','None of the Above'],
        answer:3,
        img:'Img/Question/For/8.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10 10 11 11 12','10 11 12','10 10 11 11','Compile time error'],
        answer:3,
        img:'Img/Question/For/3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Before for loop    Inside for loop    After for loop','Before for loop    After for loop','Infinite loop','Compile time error'],
        answer:2,
        img:'Img/Question/For/5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile error : variable i might not have been initialized','Compile error : variable j might not have been initialized','Both Errors','none'],
        answer:2,
        img:'Img/Question/For/10.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Inside if','Inside else','Compile time error','None of the Above'],
        answer:2,
        img:'Img/Question/IfElse/3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['error : value not mapped to a boolean','error : incompatable type : int cannot be convert to boolean','Inside else','Inside if'],
        answer:1,
        img:'Img/Question/IfElse/5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Inside if1','Inside if2','Inside if1    Inside if2'],
        answer:3,
        img:'Img/Question/IfElse/4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Inside else1','Inside else2','Inside if','Compile time error'],
        answer:0,
        img:'Img/Question/IfElse/1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Inside if1','Compile time error','Inside if2','Inside if1    Inside if2'],
        answer:2,
        img:'Img/Question/IfElse/2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10    9   8   7   6','10   9   8   7   6   5','1   2   3   4   5','Compile time error'],
        answer:1,
        img:'Img/Question/Loops/3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile Time Error','var = var','var = 10','var = 1   var = 2 ..... var = 10'],
        answer:2,
        img:'Img/Question/Loops/1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10','20','30','Compile time error'],
        answer:3,
        img:'Img/Question/Loops/2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['var = 10','var = 20','run time error','Compile time error'],
        answer:3,
        img:'Img/Question/Loops/6.jpg'
    },
    {
        q:'Choose the correct option',
        options:['var2 = 30.00','var = 30.0','run time error','Compile time error'],
        answer:1,
        img:'Img/Question/Loops/10.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10    9   8   7   6','10','5  4   3   2   1','Compile time error'],
        answer:1,
        img:'Img/Question/Loops/9.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10    9   8   7   6','5   4   3   2   1','none of these','Compile time error'],
        answer:3,
        img:'Img/Question/Loops/7.jpg'
    },
    {
        q:'Choose the correct option',
        options:['var = 30.00','var = 10','none of these','Compile time error'],
        answer:3,
        img:'Img/Question/Loops/8.jpg'
    },
    {
        q:'Choose the correct option',
        options:['10','5','1','error'],
        answer:2,
        img:'Img/Question/Loops/5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['1 error','2 error','3 error','run time error'],
        answer:2,
        img:'Img/Question/Loops/4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Exception','3','run time error'],
        answer:2,
        img:'Img/Question/BasicArray/BA9.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Garbage Value Garbage Value garbage value garbage value garbage value','null null null null','Exception'],
        answer:2,
        img:'Img/Question/BasicArray/BA2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','0 1 2','0 1 1','Exception'],
        answer:0,
        img:'Img/Question/BasicArray/BA10.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Exception','2','4'],
        answer:3,
        img:'Img/Question/BasicArray/BA8.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','0 1','[0 1]','Address of variable'],
        answer:3,
        img:'Img/Question/BasicArray/BA7.jpg'
    },
    {
        q:'Choose the correct option',
        options:['0 1 2 0','Exception','0 1 2','error'],
        answer:1,
        img:'Img/Question/BasicArray/BA4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Exception','1 2 3','0 1 2'],
        answer:0,
        img:'Img/Question/BasicArray/BA5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['error : not a statement','1 2 3 4','error : illegal start of type','Both errors'],
        answer:3,
        img:'Img/Question/BasicArray/BA6.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','10 20 30 40 50 1 2 3 4 5','run time erroe','Exception'],
        answer:1,
        img:'Img/Question/BasicArray/BA1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','0 0 1 2 3','Garbage value 1 2 3','Exception'],
        answer:1,
        img:'Img/Question/BasicArray/BA3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','3','2','4'],
        answer:0,
        img:'Img/Question/BasicArray/BA11.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','0 0 420 0','GV GV GV GV','0 0 0 0'],
        answer:1,
        img:'Img/Question/BasicArray/BA12.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','4 4 4 4','12.0 13.0 14.0 15.0','12 13 4 4'],
        answer:1,
        img:'Img/Question/BasicArray/BA13.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Run Time Exception','0.0','6.0'],
        answer:1,
        img:'Img/Question/BasicArray/BA14.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','1','2','0'],
        answer:2,
        img:'Img/Question/BasicArray/BA15.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Address','true','false'],
        answer:3,
        img:'Img/Question/BasicArray/BA16.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','3','2','GV'],
        answer:2,
        img:'Img/Question/BasicArray/BA17.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','EmptySpace EmptySpace EmptySpace','C 2 W','C garbageVal W'],
        answer:2,
        img:'Img/Question/BasicArray/BA18.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','false','0','1'],
        answer:1,
        img:'Img/Question/BasicArray/BA19.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Address','1 2 3 4 5','1'],
        answer:0,
        img:'Img/Question/BasicArray/BA20.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','1 ArrayIndexOutOfBoundException','ArrayIndexOutOfBoundException','1 8'],
        answer:1,
        img:'Img/Question/BasicArray/1D5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['error : incompatable types :int[] cannot be convarted into int','error:unexpected token','error:illegeal start of expression','error:int cannot be dereferenced'],
        answer:0,
        img:'Img/Question/BasicArray/1D4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['0 2 4 9 16','0 1 4 9 16','compile time error','0 2 4 6 8'],
        answer:2,
        img:'Img/Question/BasicArray/1D7.jpg'
    },
    {
        q:'Choose the correct option',
        options:['error : incompatable types :int[] cannot be convarted into int','No error no output','error:illegeal start of expression','error:int cannot be dereferenced'],
        answer:1,
        img:'Img/Question/BasicArray/1D1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['error : illegal start of expression','error:variable arr might not have been initaiized','0 0 0 0 0','0 1 2 3 4'],
        answer:2,
        img:'Img/Question/BasicArray/1D10.jpg'
    },
    {
        q:'Choose the correct option',
        options:['ArrayIndexOutOfBoundException','Compile time error','NegativeArraySizeException','10'],
        answer:2,
        img:'Img/Question/BasicArray/1D8.jpg'
    },
    {
        q:'Choose the correct option',
        options:['error:variable arr2 might not have benn initialized','error:unexpected token','error:int cannot be dereferenced','0'],
        answer:2,
        img:'Img/Question/BasicArray/1D2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['5 40','5 5','error:cannot assign a value to final variable length','runtime exception'],
        answer:2,
        img:'Img/Question/BasicArray/1D9.jpg'
    },
    {
        q:'Choose the correct option',
        options:['0','NegativeArraySizeException','ArrayIndexOutOfBoundException : Index 0 out of bound of length 0','compile time error'],
        answer:2,
        img:'Img/Question/BasicArray/1D3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['error:cannot find symbol','error:variable arr1 might not have been initialized ','0','No error,no output'],
        answer:1,
        img:'Img/Question/BasicArray/1D6.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','Runtime error','3','2'],
        answer:2,
        img:'Img/Question/BasicArray/cmd1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Core2Web','Compile time error','Runtime exception','3'],
        answer:2,
        img:'Img/Question/BasicArray/cmd3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','ArrayIndexOutOfBound','&','Run time error'],
        answer:1,
        img:'Img/Question/BasicArray/cmd5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['null null null','Compile time error','GV GV GV','Runtime Exception :ArrayIndexOutOfBoundException'],
        answer:3,
        img:'Img/Question/BasicArray/cmd2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['60','102030','Compile time error','Runtime error'],
        answer:1,
        img:'Img/Question/BasicArray/cmd8.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Compile time error','&','Run time error','ArrayIndexOutOfBoundException'],
        answer:1,
        img:'Img/Question/BasicArray/cmd4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Run time error','Exception','PythonJavaC','Compile time error'],
        answer:2,
        img:'Img/Question/BasicArray/cmd6.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Java Python','Core2Web  java Python','java Core2Web','Core2Web java'],
        answer:0,
        img:'Img/Question/BasicArray/cmd7.jpg'
    },
    {
        q:'Choose the correct option',
        options:['compile time error','invalid syntax','1','0'],
        answer:2,
        img:'Img/Question/BasicArray/cmd9.jpg'
    },
    {
        q:'Choose the correct option',
        options:['0','1','Error','Exception'],
        answer:1,
        img:'Img/Question/BasicArray/cmd10.jpg'
    },
    {
        q:'Choose the correct option',
        options:['2','4','1','Compile time error'],
        answer:3,
        img:'Img/Question/BasicArray/2D9.jpg'
    },
    {
        q:'Choose the correct option',
        options:['2','4','1','Compile time error'],
        answer:0,
        img:'Img/Question/BasicArray/2D1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['7','3','rum time error','Compile time error'],
        answer:1,
        img:'Img/Question/BasicArray/2D5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['3','5','15','compile time error'],
        answer:1,
        img:'Img/Question/BasicArray/2D2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['2','4','Compile time error','1'],
        answer:2,
        img:'Img/Question/BasicArray/2D10.jpg'
    },
    {
        q:'Choose the correct option',
        options:['2','4','1','Compile time error'],
        answer:3,
        img:'Img/Question/BasicArray/2D8.jpg'
    },
    {
        q:'Choose the correct option',
        options:['4','0','garbage value','runtime exception'],
        answer:3,
        img:'Img/Question/BasicArray/2D3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['arrInt[1][1]:1','arrInt[1][1]:2','arrInt[1][1]:4','compile time error'],
        answer:1,
        img:'Img/Question/BasicArray/2D6.jpg'
    },
    {
        q:'Choose the correct option',
        options:['1','0','garbage value','compile time error'],
        answer:3,
        img:'Img/Question/BasicArray/2D7.jpg'
    },
    {
        q:'Choose the correct option',
        options:['0','4','rum time error','compile time error'],
        answer:1,
        img:'Img/Question/BasicArray/2D4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['5','7','12','compile time error'],
        answer:2,
        img:'Img/Question/String/Str3.jpg'
    },
    {
        q:'Choose the correct option',
        options:['error:veriable is not compile time','error:illegal use of operator','equal','Not equal'],
        answer:3,
        img:'Img/Question/String/Str10.jpg'
    },
    {
        q:'Choose the correct option',
        options:['1','false','true','compile time error'],
        answer:2,
        img:'Img/Question/String/Str9.jpg'
    },
    {
        q:'Choose the correct option',
        options:['coreweb','core','web','compile time error'],
        answer:1,
        img:'Img/Question/String/Str1.jpg'
    },
    {
        q:'Choose the correct option',
        options:['SUN1995','sun1995','ch','compile time error'],
        answer:1,
        img:'Img/Question/String/Str4.jpg'
    },
    {
        q:'Choose the correct option',
        options:['1','false','true','compile time error'],
        answer:1,
        img:'Img/Question/String/Str8.jpg'
    },
    {
        q:'Choose the correct option',
        options:['error : symbol not found','Equal','Not Equal','error : illegal start of expression'],
        answer:1,
        img:'Img/Question/String/Str5.jpg'
    },
    {
        q:'Choose the correct option',
        options:['1','0','false','compile time error'],
        answer:2,
        img:'Img/Question/String/Str6.jpg'
    },
    {
        q:'Choose the correct option',
        options:['True 1 True 2 True 3','True 2 True 3','True 1 True 2','True 2'],
        answer:1,
        img:'Img/Question/String/Str7.jpg'
    },
    {
        q:'Choose the correct option',
        options:['979899100','"ABCD"','"abcd"','compile time error'],
        answer:3,
        img:'Img/Question/String/Str2.jpg'
    },
    {
        q:'Choose the correct option',
        options:['0 0','0 16','16 0','compile time error'],
        answer:2,
        img:'Img/Question/String/Str15.jpg'
    },
    {
        q:'Choose the correct option',
        options:['17 17','26 34','17 32','compile time error'],
        answer:1,
        img:'Img/Question/String/Str18.jpg'
    },
    {
        q:'Choose the correct option',
        options:['error:illegal start of expression','error:cannot find symbol','Hey','Bye'],
        answer:3,
        img:'Img/Question/String/Str12.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Both are not equal','Both are equal','Run time error','compile time error'],
        answer:0,
        img:'Img/Question/String/Str14.jpg'
    },
    {
        q:'Choose the correct option',
        options:['Shashi ihsahS','Shashi Shashi','ihsah Shashi','compile time error'],
        answer:3,
        img:'Img/Question/String/Str16.jpg'
    },
    {
        q:'Choose the correct option',
        options:['true','false','error:int cannot be dereferenced','error:Sysmbol not found'],
        answer:2,
        img:'Img/Question/String/Str11.jpg'
    },
    {
        q:'Choose the correct option',
        options:['null','No/Black Output','Run time error','compile time error'],
        answer:1,
        img:'Img/Question/String/Str13.jpg'
    },
    {
        q:'Choose the correct option',
        options:['true Shashi','false Shashi','false Sha','true Sha'],
        answer:2,
        img:'Img/Question/String/Str17.jpg'
    },
]