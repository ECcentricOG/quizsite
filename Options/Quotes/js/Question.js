
const quiz = [
    {
        q:'Java Byte Code is Platform Independant'
    },
    {
        q:'HAL stands for HardWare Abstraction Layer'
    },
    {
        q:'Java Versions  =   1.8 - Most Stable    1.11-2nd in Stability   1.17-LTS(Long Term Support)'
    },
    {
        q:'Java Support Unicode Format also Konwn as Internat Language'
    },   
    {
        q:'All Classes in Java are DataTypes'
    },
    {
        q:'JVM Architecture',
        img:'Img/Dia/JVMArch.jpg'
    },
    {
        q:'2D Array Internal',
        img:'Img/Dia/2DArray.jpg'
    },
    {
        q:'class interface enum everyhting in Java Program Should be in one of the three'
    },
    {
        q:'Array is a class in java and paresnt of array is object'
    },
    {
        q:'There are at least 3 connectiona to every process 1.System.in-keybord  2.System.out-Monitor   3.System.error-Monitor'
    },
    {
        q:'java.util package is introduced at version 1.5'
    },
    {
        q:'Every method push to jvm stack after its complition it gets pop when main method pops then thats the end of program'                                                                                                                                                                                          
    },
    {
        q:'if two arrays have same elements but still they have different address'
    },
    {
        q:'Integer cache has range from -128 to 127 which means two variables in between them have same value means they have same address'
    },
    {
        q:'In Java insted of pointer we call it reference & function as methods'
    },
    {
        q:'When we pass array as argument then we sends the address of that array means we do not create a new array at that point'
    },
    {
        q:'JVM Heaps maintains all the objects'
    },
    {
        q:'Garbage Collector Works only on JVM Heap section'
    },
    {
        q:'Class Loader Subsystem : 1.Checks Byte Code      2.Checks JVM and Javac version      3.Push code in method area'
    },
    {
        q:'int [][] ary = new int [2][3] ---- Valid  int [][] ary = {{10,20,30},{20,30},{30}} ----Valid    int [][] ary = new int [3][] ----Valid  int [][] ary = new int[][3]----Invalid'
    },
    {
        q:'int [][] ary = new int [3][7]    ary.length = 3'
    },
    {
        q:'PC register in JVM has man to man mapping with java Stack'
    },
    {
        q:'ascii value of "0 - 9" is 48 - 57'
    },
    {
        q:'Integer chche is Intoduced in java 1.5'
    },
    {
        q:'If object doesnot exist and we try to access that then we get NULL POINTER EXCEPTION'
    },
    {
        q:'SCP :- String Constant Pool It does not contain duplicate string all strings are unique'
    },
    {
        q:'String str = "Umesh"; --------------SCP   String str = new String("Umesh");--------------------new object on Heap'
    },
    {
        q:'identityHashCode user valueOf() method and hence always provide unique address valueOf() creates a new Object'
    },
    {
        q:'Java Strings Are Immutable'
    },
    {
        q:'identityHashCode checks for object'
    },
    {
        q:'identityHashCode of int x = 10 ; changes but identityHashCode of Integer x = 10; remains same' 
    },
    {
        q:'" == " opeator in string checks for identityHashCode'
    },
    {
        q:'.hashCode checks for content'
    },
    {
        q:'"+" operator in String calls append method from StringBuilder'
    },
    {
        q:'String str = new String("Umesh"); and StringBuffer sb = new StringBuffer("Umesh"); have different .hashCode beacaue StringBuffer has More Free space allocated'
    },
    {
        q:'StringBuffer Capacity is 16'
    },
    {
        q:'StringBuffer Capacity increase formula (curr.cap+1)*2'
    },
    {
        q:'StringBuilder is introduced in 1.5 & StringBufffer and String is intro in 1.0'
    },
    {
        q:'append is more powerful than concat'
    },
    {
        q:'StringBuffer is Synchronized But StringBuilder is Asyncronized'
    },
    {
        q:'for single thread StringBuilder is better than StringBuffer'
    },
    {
        q:'if n is the no of threads then JVM has n no og stacks and each thread has one stack to operate'
    },
    {
        q:'Constructor is used to initailized instance variable'
    },
    {
        q:'Constructor calls constructor of parent class But only exception is Object Class cause it is parent of all classes'
    },
    {
        q:'instance variable is initailized in constructor But constructor need to store somewhere hence we create object of class to create a instance variable from a static context'
    },
    {
        q:'Static variable stored in static block. Static block comes before main() method Syntax : static{}'
    },
    {
        q:'When we use identityHashCode on premitive data types it uses the valueOf() function to call the variable But if we uses the wrapper class like Integer then it already has an object hence values of indentityHashCode of primitive data types does not occour same but if we use Wrapper class then it comes same.'
    },
    {
        q:'Staric Block is Stored on Method Area'
    },
    {
        q:'Impossible to create object at run time'
    },
    {
        q:'Java doesnot support copy consatuctor like CPP'
    },
    {
        q:'Interface does not contain constructor'
    },
    {
        q:'if user does not add constructor then compiler adds it in the code'
    },
    {
        q:'Construcotr is called automatically when obj is created'
    },
    {
        q:'We can not call System.out.println() outside of method'
    },
    {
        q:'Method area stores all .class files of all classes used in code'
    },
    {
        q:'If we does not want anyone to create object of our class at that time we make our class constructor private'
    },
    {
        q:'value of static variable is constant for all objects of that class means if one obj makes the change is static variable then all objects will gets affected'
    },
    {
        q:'Value of instance variable is diffent for differnt objects og that class means if one objects makes vhange it will not affect value in other object'
    },
    {
        q:'We can use static variable by 1) using object of the class  2) class Name'
    },
    {
        q:'Byte code in method area gets devided into variables and methods seperate part for each method'
    },
    {
        q:'Class object stores the address of byte code of method accessable to that object'
    },
    {
        q:'we cannot write static varibale in instance method'
    },
    {
        q:'Class object contain address of special structure and special structure is actually on method area which stores address of static block and static methods in that class'
    },
    {
        q:'Static Block is the first one who goes on the java stack and it\'s static frame get\'s pop at that time main method stack frame get push on java stack'
    },
    {
        q:'Whenever static block goes on java static it initializes the all static varibales until that they are decleard but has no value'
    },
    {
        q:'Java support global static variables if it is non static then it cannot be class variable'
    },
    {
        q:'Insatance block merges in constructor'
    },
    {
        q:'Priority Order :- Static Variable > Static Block > Static Method > Instance Variable > Instance Block > Constructor > Instance Method'
    },
    {
        q:'All Instance methods has this parameter'
    },
    {
        q:'Method table is created at compile time it contain method signature like void fun(int x) -> fun(int)'
    },
    {
        q:'Demo obj = new Demo() ==> Demo(obj)'
    },
    {
        q:'Demo class\'s instance method fun() ==> fun(Demo this)'
    },
    {
        q:'"this" is used to identify instance varibale'
    },
    {
        q:'Setter Method ==> Takes Data & Set Value  Getter Method ==> Prints Data'
    },
    {
        q:'Default and Public are only to modifiers for Class'
    },
    {
        q:'Constructor and Class hava same access specifier if access specifier is assign to class'
    },
    {
        q:'Composition ==> creates 2 objects but inheritance creates only one obj parents class works on child'
    },
    {
        q:'One object of last class can access all the members of all above class'
    },
    {
        q:'We can create child obj with parent class but it can only access methos which is common in them'
    },
    {
        q:'Special Structure Contain pointer to Parent\'s Special Structure which is used for static in inheritance'
    },
    {
        q:'this is obj which has same value as object that we created all Parents and Child uses the same this'
    },
    {
        q:'Super is a non static varible and a Special Keyword in Java'
    },
    {
        q:'Return Type doesnot matter in Overloading but it mattters in Overirding'
    },
    {
        q:'If we create child object with parent refernce then with that object we can only access methods which are present in parent class but childs methods get\'s executed which is overirding'
    },
    {
        q:'Child methods have more priority than Parent methods'
    },
]